-- Membuat database
-- CREATE DATABASE db_game;
-- create table users
CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
-- create table profiles
CREATE TABLE profiles (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id BIGINT NOT NULL,
  name VARCHAR(50) NOT NULL,
  thumbnail VARCHAR(255) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
-- create table questions
CREATE TABLE questions (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  question TEXT NOT NULL,
  value INTEGER
);
-- create table answers
CREATE TABLE answers (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  question_id BIGINT NOT NULL,
  answer TEXT NOT NULL,
  is_correct BOOLEAN NOT NULL
);
-- create table histories
CREATE table histories (
  id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id BIGINT NOT NULL,
  high_score INTEGER,
  current_score INTEGER NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO users VALUES
('1', 'admin', 'admin', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('2', 'user', 'user', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('3', 'umar', 'umar', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('4', 'deny', 'deny', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('5', 'caknan', 'caknan', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('6', 'mantan', 'mantan', '2020-10-01 00:00:00', '2020-10-01 00:00:00');

INSERT INTO profiles VALUES
('1', '1', 'admin', 'admin.png', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('2', '2', 'user', 'user.png', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('3', '3', 'umar', 'umar.png', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('4', '4', 'deny', 'deny.png', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('5', '5', 'caknan', 'caknan.png', '2020-10-01 00:00:00', '2020-10-01 00:00:00'),
('6', '6', 'mantan', 'mantan.png', '2020-10-01 00:00:00', '2020-10-01 00:00:00');
INSERT INTO questions VALUES
(1, 'Berapakah jumlah kaki bebek ?', 10),
(2, 'Apakah kita harus balikan dengan mantan ? aku saranin sih jangan ...', 10),
(3, '1 + 2 = ?', 10),
(4, 'Siapakah penjahit bendera merah putih ?', 10),
(5, 'Orang yang lebih tua harus di ....?', 10),
(6, 'Warna hijau adalah gabungan dari warna ?', 10),
(7, 'Kucing apa yang pengen tidur ?', 10),
(8, 'Kapan Indonesia merdeka ....?', 10),
(9, 'Siapakah pengarang lagu sang dewi ?', 10),
(10, 'Arti dari lagu sugeng dalu adalah ?', 10);
INSERT INTO answers VALUES 
('1', 1, '3', false),
('2', 1, '2', true),
('3', 2, 'Ya', false),
('4', 2, 'Tidak', true),
('5', 3, '3', true),
('6', 3, '12', true),
('7', 4, 'Fatmawati', true),
('8', 4, 'Ningsih', false),
('9', 5, 'hormati', true),
('10', 5, 'segani', false),
('11', 6, 'merah dan putih', false),
('12', 6, 'biru dan kuning', true),
('13', 7, 'kucing diam', false),
('14', 7, 'kucing antuk', true),
('15', 8, '17 Agustus 1945', true),
('16', 8, '17 Agustus 1946', false),
('17', 9, 'Lyodra & Andi Rianto', false),
('18', 9, 'Titi DJ & Andi Rianto', true),
('19', 10, 'Selamat pagi', false),
('20', 10, 'Selamat malam', true);

-- Perintah untuk mulai
CREATE OR REPLACE PROCEDURE mulai(
  id_user INT
)
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO histories (user_id, high_score, current_score) VALUES (id_user, 0, 0);
  COMMIT;
END$$;

-- PERINTAH UNTUK MENJAWAB SOAL DAN MENGUPDATE NILAI JIKA BENAR
CREATE or REPLACE PROCEDURE choiced(
  id_user INT,
  id_soal INT,
  answer_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN

-- Cek apakah jawaban benar
  IF EXISTS (SELECT * FROM ANSWERS WHERE question_id = id_soal AND id = answer_id AND is_correct = true) THEN
    UPDATE histories SET current_score = current_score + 10 WHERE user_id = id_user;
  END IF;
  -- ceck high score
  IF EXISTS(SELECT * FROM histories WHERE user_id = id_user AND current_score > high_score) THEN
    UPDATE histories SET high_score = current_score WHERE user_id = id_user;
  END IF;
  COMMIT;
END$$;
-- karena di prosedure di set nilai default 10 jika jawaban benar maka
ALTER TABLE questions DROP COLUMN value;
-- CREATE VIEW untuk menampilkan soal dan pilihan jawaban
CREATE VIEW kuis AS SELECT question, answer FROM questions LEFT JOIN answers ON questions.id = answers.question_id;
-- PERINTAH UNTUK MENAMPILKAN SOAL dan pilihan jawaban
SELECT * FROM kuis;
-- create view report histories
CREATE VIEW report_histories AS SELECT profiles.name, histories.high_score, histories.current_score FROM users LEFT JOIN profiles ON users.id = profiles.user_id LEFT JOIN histories ON users.id = histories.user_id;
-- Report histories
select * from report_histories;

-- Menghapus soal dan pilihan jawaban nomor 1
DELETE FROM answers WHERE question_id = 1;
DELETE FROM questions WHERE id = 1;